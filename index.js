const express = require('express');

const server = express();

server.use(express.json());

const projeto = [];

function logRequest(req, res, next) {

  console.count ("Número de requisições");

  return next();

}

server.use(logRequest);

function checarID (req,res, next) {

  const { id } = req.params;
  const project = projeto.find (p => p.id == id);

  if (!project) {
    return res.status(400).json({ erro : "Projeto não existe"});

  }

  return next();
  

}



server.post('/projects', (req,res) => {

  const { id } = req.body;
  const { title } = req.body;

  const project = {
    id,
    title,
    tasks: []

  };

  projeto.push(project);

  return res.json(projeto);
  

})

server.get('/projects', (req, res) => {

  return res.json(projeto);

})


server.put('/projects/:id', checarID, (req,res) => {
  
  const { id } = req.params;
  const { title } = req.body;

  const project = projeto.find(p => p.id == id);

  project.title = title;

  return res.json(projeto);

}) 

server.delete('/projects/:id',checarID , (req,res) => {

  const { id } = req.params;
  
  const project = projeto.findIndex(p => p.id == id);

  projeto.splice(project, 1);

  return res.send();

})

server.post('/projects/:id/tasks', checarID , (req,res) => {

  const { id } = req.params
  const { title } = req.body;

  const project = projeto.find(p => p.id == id);

  project.tasks = title;

  return res.json(projeto);

})


server.listen(3000);